# Roadmap to Statistics (in work) for Data Consulting

## Most important thing to consider
1. Time
How long will it take? 
(MLatWork) So
    1. Build hypothesis with clear problem definition / and just start building MVP
    2. Check what algorithm we will use (don't be afraid not using model)
    3. prediction performance + KPI

2. System / Business

## What we have to learn from statistics
1. Data
2. ML
3. DL

## Most important thing? verification / inspection

- A/B testing
- P-hacking


# Process of analytics
- Cross Industry Standard Process for Data Mining (CRISP-DM)
    1. Business Understanding
        - Define goal 
    2. Data Understanding
        - making right plan to collect the data
        - understanding
    3. Data Preparation
        - data able to be process
        - missing / outlier / normalization
        - categorical > numerical data
    4. Modeling
        - divide data into test, train
    5. Evaluation
        - testing to see model is doing right thing
        - moving back to the modeling step
    6. Deployment

# Course Design
- Linear regression / polynomial regression
- Logistic Regression >>> Lasso >>> Ridge >>> Elastic net
- Decision Tree >>> Random Forest >>> GBDT
- SVM SVR
- k-NN
- Clustering / PCA(Dimensionality reduction)
- Perceptron >>> Multilayer Perceptron >>> NN (Deep) >>> RNN >>> CNN

- Recommendation
- Anomaly detection
- Pattern Mining(Association rule)
- Reinforcement learning



- A/B testing(P-hacking)




