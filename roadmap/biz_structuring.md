# Structuring
- Basis of case study
- Practice every day working (related to everything)
    1. Making plan
        - Time (L-T, S-T) to the goal
        - Individual vs Team
    2. Clear communication (Build your own structure and communicate with seniors)
    3. Conducting the research
    4. Making one page of the PPT

Example
1. finding the price information of the product > crawling the list > making statistics (Considerations: time, skill, needs)
2. Categorizing the list of issues (Building structure of priority, reasons to choose it, reference points >>> making as easiest for the senior to choose right item)
3. Client urgent request (managing my own task, we should never over work, dealing)



