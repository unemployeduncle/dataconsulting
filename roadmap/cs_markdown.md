# Markdown language summary

## Introduction

Simple guideline for Markdown basics
https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet

Exercise
https://commonmark.org/help/tutorial/index.html


Heavily used
## Header
# Title1
## Title2
### Title3
#### Title 4
normal text
##### smaller than normal text
######

Heavily used
## List with Number
1.
1.
1.

## List without Number
*
    *
        *

can use anything
*
+
-

## Indentation
use 4 space or tab...


Heavily used
## Code block (Backtick on left side of the keyboard!)
```
```

```python
a = 1
b = 2
```

## Horizontal line
---

## Quotation
>

Heavily used
## Link
Using it inside the sentence
1. [Text](Link.com)
2. 
[In the text][TitleID]

Manage Links
[TitleID]: www.ddd.com

## Emphasis
*word*
**word*
_word_
__word__

## image (This link should be the right address of the image file!)
Use the image
![text for explanation][TitleID]

Manage the links of the picture
[TitleID]: www.ddd.com
