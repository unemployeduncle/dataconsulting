# Roadmap for Git

How long will it take? 

## Basic Concepts
Git is the distributed version control system
first condition comes first
Repository: projects managed by git
Commit: Specific timing of the version(=History)
Branch: independent history

## Install
1. Git
2. Open Gitbash or Terminal

## Choose right site for version control
1. Github
2. Gitlab
3. Bitbucket

## Starting git
### Step 1. add configuration informations
'''
git config --global user.name "your_name"
git config --global user.name
git config --global user.email "your_email@example.com"
git config --global user.email
git config --list
'''

### Step 2. SSH Key Creation
ssh-keygen -t ed25519 -C "email@example.com"
Mac: pbcopy < ~/.ssh/id_ed25519.pub
window: cat ~/.ssh/id_ed25519.pub | clip

### Step 3. Create folder or go to the folder where to clone it
#### Bash
cd
ls

### clone the repository
git clone address_of_the_repository

### Step 4. Using Git
#### Step 4-1. Uploading the changes (git push)
git add .
git commit -m "message of what you did"
git push origin master

#### Step 4-2. Downloading the changes (git pull)
git pull origin master




## Tips
to-do = add contents of commit

How to write great commits
1. write what you changed
2. write what had been changed
3. add more information of it (what is worried, whats the future work, etc)
This rule make me to organize the process


